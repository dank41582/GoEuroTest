package com.goeuro.test;

import java.io.IOException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class CityJsonDeserializer extends JsonDeserializer {

	@Override
	public City[] getObjectFromJson(URL url) {
		
		//read values from ObjectMapper
		
		City[] cities = null;
		
		try {
        	
			cities = getObjectMapper().readValue(url, City[].class);
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cities;
	}

}
