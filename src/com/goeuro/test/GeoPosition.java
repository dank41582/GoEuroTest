package com.goeuro.test;


//POJO representation of geopositions that we will import (which are provided for each city)
public class GeoPosition{
	
	private double latitude;
	private double longitude;
	
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}