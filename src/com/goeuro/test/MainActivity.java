package com.goeuro.test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;


/*
 * Programmer: Daniel Krysztopa
 * 
 * Read in a JSON array that contains JSON objects as elements.
 * Each object, among other keys, has a name and a geo_position key. 
 * The geo_position key is an object with latitude and longitude fields. 
 * If no matches are found an empty JSON array is returned.
 */

public class MainActivity {
	
	private static String GO_EURO_URL = "http://api.goeuro.com/api/v2/position/suggest/en/";
	private static String FILE_PATH = ".";
	private static String FILE_NAME = "GoEuroLocations.csv";
	private static String DELIMITER = ",";

	/**
	 * @param args
	 */
	public static void main(String[] args){
		
		//Components:
		// 1. get the url
		// 2. get/parse the json
		// 3. create output
		// 4. output to file
		
		
		//user passes in city name as first argument
		if (args.length > 0){
			
			String cityName = args[0];
			
			// 1. append city name to url - we will use the resulting url to query for data for the given city
	        URL urlWithCity = null;
			try {
				urlWithCity = new URL(GO_EURO_URL + cityName);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

	        // 2. go to url to retrieve city data for user provided city name - convert json response to java objects
	        CityJsonDeserializer cityJsonDeserializer = new CityJsonDeserializer();
	        City[] cityObjs = cityJsonDeserializer.getObjectFromJson(urlWithCity);
	        		
			
			// 3. extract specific content from each Java object and create a delimited line describing each object's properties
			//include the following properties in the output: _id, name, type, latitude, longitude
	        String delimitedContent = CityOutputCreator.getCitiesOutputAsDelimitedString(cityObjs, DELIMITER);
	        
	        
			// 4. put delimited content into a file at given location
			File outputFile = FileUtils.createFileFromContent(FILE_PATH, FILE_NAME, delimitedContent);
			
			
			//for reference, print file path if file exists
			if (outputFile.exists())
				System.out.println("Output file path <"+outputFile.getAbsolutePath()+">");
			else
				System.out.println("Output file doesnt exist");
		}
		else{
			System.out.println("Processing aborted. Please provide a city name as an argument");
		}
		
	}
}
